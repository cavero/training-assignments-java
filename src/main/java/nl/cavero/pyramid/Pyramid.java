package nl.cavero.pyramid;

/**
 * The purpose of this exercise is to write unit tests to find the error(s) in the program.
 *
 * The program returns the two numbers below the given number in a pyramid like the one given below:
 *
 *              1
 *           2     3
 *        4     5     6
 *     7     8     9    10
 *
 * So, when you enter 2, it returns 4 and 5; and when you enter 5 it returns 10 and 11. It does this for any given number,
 * and the pyramid is infinitely high and infinitely wide.
 * Or, at least, it's supposed to return the correct number...
 *
 * Write unit tests to the program to prove what it does right and what it does wrong and why. (Do not fix the program,
 * just prove it's wrong sometimes.)
 *
 * Hint: there are several things wrong!
 *
 */
public class Pyramid {
    private final int toFind;

    public Pyramid(int toFind) {
        this.toFind = toFind;
    }

    public int[] solve() {
        int row = 0;
        int countedElements = 0;

        while(countedElements <= toFind) {
            row++;
            countedElements += row;
        }

        int left = toFind + row;
        int right = left + 1;
        return new int[] { left, right };
    }

}
